# search-engine-exercise1

## Installation

1. install dependencies
2. cd into project dir
3. inside [start.py](../blob/master/start.py) on line 10 you set the path to the dataset directory.
   
> Example datasets are at `./files` and `./files_full` 
> **please note:** `./full_list` directory contains many files and indexing currently a takes long time. for convinience, `./files` is a small debugging dataset.

4. inside [start.py](../blob/master/start.py) on line 11 you set language of the dataset.
5. to start run `python start.py`

## Dependencies

- install python >= 3.5
- install python dependencies using `pip install -r requirements.txt`
- Before first run you have to download nltk data to use in this script 


> ## **NLTK data download**
> ### __Windows__ 
>   run `python -c 'import nltk; nltk.download()'`
> ### __Mac / Linux / Unix__
>   run `python -m nltk.downloader all`