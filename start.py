from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import *
from operator import itemgetter 
import string 
import bisect
import os

LANGUAGE  = 'english'
FILES_PATH = './files'

NORMALS = {
  1: "one",
  2: "two",
  3: "three",
  4: "four",
  5: "five"
}

class Tokenizer:
  "tokenizer class for dataset"
  
  def __init__(self, str):
    self.words_tokenize = word_tokenize
    self.stopwords = set(stopwords.words(LANGUAGE) + list(string.punctuation))
    self.data = self.words_tokenize(str)
    self.stemmer = PorterStemmer()
    self.filtered_words = []

    for word in self.data:
      if word not in self.stopwords:
        if word in NORMALS.keys():
          word = NORMALS[word]        
        self.filtered_words.append(self.stemmer.stem(word.lower()))

  def get_filtered_words(self):
    return self.filtered_words

class Dataset:
  "this manages the dataset object and holds all indices"
  def __init__(self, path):
    self.files = []
    self.collect(path)
    self.tokenize_dataset()

  def get_files(self):
    "return files"
    return self.files

  def collect(self, path):
    "collect file list from target into dataset object"
    for root, dir, file_list in os.walk(path):
      for ind, a in enumerate(file_list):
        d = {"index": ind, "filename": os.path.join(root,a)}
        self.files.append(d)

  def tokenize_data(self, data):
    "return the tokenized term in string"
    tokenizer = Tokenizer(data)
    return tokenizer.get_filtered_words()

  def tokenize_dataset(self):
    "iterate dataset and tokenize file terms"
    for file in self.files:
      try:
        with open(file["filename"], 'r') as f:
          file["terms"] = self.tokenize_data(f.read())
      except UnicodeDecodeError as e:
        print("ERROR: cannot decode file encoding at %s, error: %s" % (file['filename'], e))
      except Exception as e:
        print("skipping file %s due to error: %s" % (file['filename'], e) )
  
  def get_filenames(self, indices):
    "return list of requested indices from dataset"
    files = itemgetter(*indices)(self.files)
    if (type(files) == dict):
      return [files['filename']]
    return [ d['filename'] for d in files]

class InvertedIndex:
  "class to manage the inverted index"

  def __init__(self, dataset):
    self.index = {}
    files = dataset.get_files()
    for file in files:
      if "terms" not in file:
        print("ERROR: on file %s, could not get terms" % file)
        continue
      self.index_document(file["terms"], file["index"])

  def map_term_in_index(self, l, doc_id):
    "insert file index into term key in inverted index"
    if doc_id not in l:
      bisect.insort(l, doc_id)

  def index_document(self, doc, doc_id):
    "iterate file terms and map into inverted index"
    for term in doc:
      if (term not in self.index.keys()):
        self.index[term] = []
      self.map_term_in_index(self.index[term], doc_id)
    
  def print_inverted_index(self):
    "print the inverted index"
    for key in sorted(self.index.keys()):
      print('%s: %s' % (key, self.index[key]))

  def intersect_term_list(self, query):
    "tokenize query string and return matching file indices from inverted index"
    t = Tokenizer(query)
    terms = t.get_filtered_words()

    def intersect(l1, l2):
      res = []
      for i in l1:
        if i in l2:
          res.append(i)
      return res

    sections = []

    for term in terms:
      if term in self.index.keys():
        sections.append(self.index[term])

    if len(sections) == 0:
      print("Result: %s Not Found in index" %  query )
      return None

    if len(sections) < len(terms):
      print("Result: cloud not find a match for all terms in search query")
      return None

    res = sections[0]
    for sec in sections[1:]:
      res = intersect(res, sec)

    # result = set(sections[0]).intersection(*sections)
    return res

def get_search_query():
  "get user input search string"
  return input("Please enter search query: ")

if __name__ == "__main__":
  # initialize the dataset object
  dataset = Dataset(FILES_PATH)

  # initialize the inverted index
  inv_index = InvertedIndex(dataset)

  # user input loop
  cont = True
  while (cont):
    
    #  get search query from user
    query = get_search_query()

    #  get matching files index
    indices = inv_index.intersect_term_list(query)

    if (indices):

      #  print results
      print("Found \"%s\" in dataset files at index: %s" % ( query, indices ) )
      files = dataset.get_filenames( list(indices) )
      print("Filnames: " +  ', '.join( files ) )

    # validate user input
    ans_valid = False
    while(not ans_valid):
      ans = input("search again? (y/n): ")
      if (ans.lower() == 'n'):
        ans_valid = True
        cont = False
      elif (ans.lower() == 'y'):
        ans_valid = True
      else:
        print("ERROR: invalid value in answer please enter y / n")
